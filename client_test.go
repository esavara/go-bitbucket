package bitbucket

import (
	"os"
	"strings"
	"testing"
)

func TestBasicAuth(t *testing.T) {

	c := NewBasicAuth("example", "password")

	if c.Auth.user != "example" {
		t.Error("username is not equal")
	}

	if c.Auth.password != "password" {
		t.Error("password is not equal")
	}
}

func TestOAuth(t *testing.T) {
	c := NewOAuth(
		os.Getenv("BBTEST_CLIENT"),
		os.Getenv("BBTEST_SECRET"),
		strings.Split(os.Getenv("BBTEST_SCOPES"), ","))
	ro := &RepositoryOptions{
		Owner:     "esavara",
		Repo_slug: "go-bitbucket",
	}
	// Recoge información de su propio repositorio de código
	_, err := c.Repositories.Repository.Get(ro)
	if err != nil {
		t.Fatalf("Hubo un error al pedir datos del repositorio %s/%s: %s", ro.Owner, ro.Repo_slug, err)
	}
}

func TestOAuthFail(t *testing.T) {
	c := NewOAuth(
		"xxxx",
		"yyyy",
		[]string{"x", "y", "z"})
	ro := &RepositoryOptions{
		Owner:     "esavara",
		Repo_slug: "go-bitbucket",
	}
	// Recoge información de su propio repositorio de código
	_, err := c.Repositories.Repository.Get(ro)
	if err == nil {
		t.Fatalf("Pedir datos del repositorio %s/%s no causa fallos: %s", ro.Owner, ro.Repo_slug, err)
	}
}
