package bitbucket

type Diff struct {
	c *Client
}

func (d *Diff) GetDiff(do *DiffOptions) (interface{}, error) {
	url := d.c.requestURL("/repositories/%s/%s/diff/%s", do.Owner, do.Repo_slug, do.Spec)
	return d.c.execute("GET", url, "")
}

func (d *Diff) GetPatch(do *DiffOptions) (interface{}, error) {
	url := d.c.requestURL("/repositories/%s/%s/patch/%s", do.Owner, do.Repo_slug, do.Spec)
	return d.c.execute("GET", url, "")
}
