package bitbucket

type Teams struct {
	c *Client
}

func (t *Teams) List(role string) (interface{}, error) {
	url := t.c.requestURL("/teams/?role=%s", role)
	return t.c.execute("GET", url, "")
}

func (t *Teams) Profile(teamname string) (interface{}, error) {
	url := t.c.requestURL("/teams/%s/", teamname)
	return t.c.execute("GET", url, "")
}

func (t *Teams) Members(teamname string) (interface{}, error) {
	url := t.c.requestURL("/teams/%s/members", teamname)
	return t.c.execute("GET", url, "")
}

func (t *Teams) Followers(teamname string) (interface{}, error) {
	url := t.c.requestURL("/teams/%s/followers", teamname)
	return t.c.execute("GET", url, "")
}

func (t *Teams) Following(teamname string) (interface{}, error) {
	url := t.c.requestURL("/teams/%s/following", teamname)
	return t.c.execute("GET", url, "")
}

func (t *Teams) Repositories(teamname string) (interface{}, error) {
	url := t.c.requestURL("/teams/%s/repositories", teamname)
	return t.c.execute("GET", url, "")
}
