package bitbucket

import (
	"context"
	"encoding/json"
	"fmt"

	"golang.org/x/oauth2/clientcredentials"

	"io/ioutil"
	"net/http"
	"strings"

	bbendpoint "golang.org/x/oauth2/bitbucket"
)

// Client holds other structure which logically follow from the top client structure
type Client struct {
	Auth         *auth
	Users        users
	User         user
	Teams        teams
	Repositories *Repositories
}

// auth holds authentication information or an OAuth "two-legged OAuth 2.0" client
type auth struct {
	oclient        *http.Client
	user, password string
}

// NewOAuth returns a *Client which can make use of OAuth 2 authentication and
// do requests to Bitbucket's API.
func NewOAuth(client, secret string, scopes []string) *Client {
	twolegsconf := &clientcredentials.Config{
		ClientID:     client,
		ClientSecret: secret,
		TokenURL:     bbendpoint.Endpoint.TokenURL,
		Scopes:       scopes,
	}
	a := &auth{
		oclient: twolegsconf.Client(context.Background()),
	}
	return injectClient(a)
}

// NewBasicAuth returns a *Client which uses the username and password to do
// requests to Bitbucket's API.
func NewBasicAuth(u, p string) *Client {
	a := &auth{user: u, password: p}
	return injectClient(a)
}

func (c *Client) execute(method, url, text string) (interface{}, error) {
	var client *http.Client
	body := strings.NewReader(text)
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	// indica al servidor el tipo de respuesta que esperamos
	req.Header.Set("Content-Type", "application/json")
	if c.Auth.oclient == nil {
		if c.Auth.user != "" && c.Auth.password != "" {
			req.SetBasicAuth(c.Auth.user, c.Auth.password)
		}
		client = new(http.Client)
	} else {
		// Usamos nuestro cliente creado con la autenticación OAuth 2
		// de "dos patas"
		client = c.Auth.oclient
	}
	// Realizamos la petición a la API
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result interface{}
	json.Unmarshal(buf, &result)

	return result, nil
}

func (c *Client) requestURL(template string, args ...interface{}) string {

	if len(args) == 1 && args[0] == "" {
		return GetApiBaseURL() + template
	}
	return GetApiBaseURL() + fmt.Sprintf(template, args...)
}
